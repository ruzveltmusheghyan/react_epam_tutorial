import Board from "./Board";
import React from "react";
import { calculateWinner } from "../helpers/calculateWinner";
import Move from "./Move";
class Game extends React.Component {
  state = {
    history: [
      {
        squares: Array(9).fill(null),
      },
    ],
    stepNumber: 0,
    xIsNext: true,
    moveHistory: null,
    isDesc: true,
  };

  getMoves = (
    history = this.state.history,
    stepNumber = this.state.stepNumber
  ) => {
    let moves = history.map((_, move) => {
      const desc = move ? "Go to move #" + move : "Go to game start";
      return (
        <Move
          key={move}
          move={move}
          desc={desc}
          jumpTo={this.jumpTo}
          stepNumber={stepNumber}
        />
      );
    });
    if (this.state.isDesc) {
      return moves;
    }
    return moves.sort((a, b) => b.props.move - a.props.move);
  };

  jumpTo = (step) => {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0,
      moveHistory: this.getMoves(this.state.history, step),
    });
  };

  handleAsc = (arr) => {
    this.setState({
      moveHistory: arr.sort((a, b) => b.props.move - a.props.move),
      isDesc: false,
    });
  };

  handleDesc = (arr) => {
    this.setState({
      moveHistory: arr.sort((a, b) => a.props.move - b.props.move),
      isDesc: true,
    });
  };

  handleClick = (i) => {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    if (this.state.stepNumber + 1 === 10) {
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    const newHistory = history.concat([{ squares: squares }]);
    this.setState({
      history: newHistory,
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      moveHistory: this.getMoves(newHistory, history.length),
    });
  };

  componentDidMount() {
    this.setState({
      moveHistory: this.getMoves(),
    });
  }

  render() {
    const { history, xIsNext, moveHistory, stepNumber } = this.state;
    const current = history[stepNumber];
    const winner = calculateWinner(current.squares);

    let status = "Next player: " + (xIsNext ? "X" : "O");
    if (winner) status = "Winner: " + winner.winner;
    if (!winner && stepNumber === 9) status = "Draw";
    return (
      <div className="game">
        <div className="game-board">
          <Board
            winnerLines={winner ? winner.lines : null}
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moveHistory}</ol>
          <button onClick={() => this.handleAsc(moveHistory)}>asc</button>
          <button onClick={() => this.handleDesc(moveHistory)}>desc</button>
        </div>
      </div>
    );
  }
}

export default Game;
