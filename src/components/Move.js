import React from "react";
class Move extends React.Component {
  render() {
    const { move, desc, jumpTo, stepNumber } = this.props;

    return (
      <li>
        <button onClick={() => jumpTo(move)}>
          <span className={`${move === stepNumber ? "bold" : null}`}>
            {desc}
          </span>
        </button>
      </li>
    );
  }
}

export default Move;
