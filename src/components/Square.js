import React from "react";

class Square extends React.Component {
  render() {
    return (
      <button
        className={
          this.props.winnerLines &&
          this.props.winnerLines.includes(this.props.i)
            ? "red square"
            : "square"
        }
        onClick={() => this.props.onClick()}
      >
        {this.props.value}
      </button>
    );
  }
}

export default Square;
