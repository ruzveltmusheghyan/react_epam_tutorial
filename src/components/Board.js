import React from "react";
import Square from "./Square";
class Board extends React.Component {
  render() {
    console.log(this.props);
    let squares = [];
    for (let i = 0; i < 9; i++) {
      squares.push(
        <Square
          key={i}
          i={i}
          winnerLines={this.props.winnerLines}
          value={this.props.squares[i]}
          onClick={() => this.props.onClick(i)}
        />
      );
    }
    squares = squares.reduce((aggr, el, index) => {
      const splitIndex = Math.floor(index / 3);
      if (!aggr[splitIndex]) {
        aggr[splitIndex] = [];
      }
      aggr[splitIndex].push(el);
      return aggr;
    }, []);

    return (
      <div>
        {squares.map((el, i) => {
          return (
            <div key={i} className="board-row">
              {el.map((square) => {
                return square;
              })}
            </div>
          );
        })}
      </div>
    );
  }
}

export default Board;
